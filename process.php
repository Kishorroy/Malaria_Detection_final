<html>
<head>
    <title>
        Students Information
    </title>
    <style>
        .container{
            height:600px;
            width: 600px;
            margin-left: 400px;
            margin-top: 50px;
            background: linear-gradient(darkorange,crimson,red);
        }
    </style>
</head>
<body style="background: linear-gradient(white,aquamarine,darkturquoise);">
<div class="container">
<?php
if (isset($_POST)){
    $objPerson=new Person();
    $objPerson->setData($_POST);
    echo "<h1 style='color: white;'><center>Student Personal Information</center></h1> <hr></hr>";
    $objPerson->personalinfo();
    echo "<h1 style='color: white;'><center>Student Marks Information</center></h1> <hr></hr>";
    echo "<b><p style='color: white; margin-left: 220px;'>Subject&nbsp;&nbsp;&nbsp; Marks &nbsp;&nbsp;&nbsp; Grade<p></b>";
    echo "<center>";
    $objPerson->banglaoutput();
    echo "<br>";
    $objPerson->englishoutput();
    echo "<br>";
    $objPerson->mathoutput();
    echo "<br>";
    $objPerson->ictoutput();
    echo "<br><br><br><br>";
    $objPerson->finalresult();
    echo "</center>";
}

class Person{
    protected $name;
    protected $studentID;
    protected $dob;
    protected $gender;
    protected $bangla;
    protected $english;
    protected $math;
    protected $ict;

    public function banglaoutput(){

        if($this->getBangla()>=80) {
            echo "<p style='color: white'>";
            echo"Bangla &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getBangla() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A+";
        }
        elseif($this->getBangla()>=70 && $this->getBangla()<80) {
            echo "<p style='color: white'>";
            echo"Bangla &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getBangla() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A";
        }
        elseif($this->getBangla()>=60 && $this->getBangla()<70) {
            echo "<p style='color: white'>";
            echo"Bangla&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getBangla() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A-";
        }
        elseif($this->getBangla()>=50 && $this->getBangla()<60) {
            echo "<p style='color: white'>";
            echo"Bangla&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getBangla() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B";
        }
        elseif($this->getBangla()>=40 && $this->getBangla()<50) {
            echo "<p style='color: white'>";
            echo"Bangla&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getBangla() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C";
        }
        elseif($this->getBangla()>=33 && $this->getBangla()<40) {
            echo "<p style='color: white'>";
            echo"Bangla&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getBangla() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D";
        }
        else{
            echo "<p style='color: white'>";
            echo"Bangla&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getBangla() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;F";
        }

    }
    public function englishoutput(){
        if($this->getEnglish()>=80) {
            echo"English&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getEnglish() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A+";
        }
        elseif($this->getEnglish()>=70 && $this->getEnglish()<80) {

            echo"English&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getEnglish() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A";
        }
        elseif($this->getEnglish()>=60 && $this->getEnglish()<70) {

            echo"English&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getEnglish() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A-";
        }
        elseif($this->getEnglish()>=50 && $this->getEnglish()<60) {

            echo"English&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getEnglish() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B";
        }
        elseif($this->getEnglish()>=40 && $this->getEnglish()<50) {

            echo"English&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getEnglish() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C";
        }
        elseif($this->getEnglish()>=33 && $this->getEnglish()<40) {
            echo"English&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getEnglish() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D";
        }
        else{
            echo"English&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getEnglish() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;F";
        }
    }
    public function mathoutput(){

        if($this->getMath()>=80) {
            echo"Math&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getMath() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A+";
        }
        elseif($this->getMath()>=70 && $this->getMath()<80) {
            echo"Math&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getMath() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A";
        }
        elseif($this->getMath()>=60 && $this->getMath()<70) {
            echo"Math&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getMath() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A-";
        }
        elseif($this->getMath()>=50 && $this->getMath()<60) {
            echo"Math&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getMath() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B";
        }
        elseif($this->getMath()>=40 && $this->getMath()<50) {
            echo"Math&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getMath() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C";
        }
        elseif($this->getMath()>=33 && $this->getMath()<40) {
            echo"Math&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getMath() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D";
        }
        else{
            echo"Math&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getMath() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;F";
        }
    }
    public function ictoutput(){

        if($this->getIct()>=80) {
            echo"ICT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getIct() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A+";
        }
        elseif($this->getIct()>=70 && $this->getIct()<80) {
            echo"ICT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getIct() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A";
        }
        elseif($this->getIct()>=60 && $this->getIct()<70) {
            echo"ICT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getIct() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A-";
        }
        elseif($this->getIct()>=50 && $this->getIct()<60) {
            echo"ICT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getIct() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B";
        }
        elseif($this->getIct()>=40 && $this->getIct()<50) {
            echo"ICT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getIct() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C";
        }
        elseif($this->getIct()>=33 && $this->getIct()<40) {
            echo"ICT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getIct() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D";
        }
        else{
            echo"ICT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getIct() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;F";
        }
    }
    public function finalresult(){
        {if($this->getBangla()>=80) {
            $a=5.00;
        }
        elseif($this->getBangla()>=70 && $this->getBangla()<80) {

            $a=4.00;//echo"Bangla &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getBangla() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A";
        }
        elseif($this->getBangla()>=60 && $this->getBangla()<70) {

            $a=3.50;//echo"Bangla&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getBangla() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A-";
        }
        elseif($this->getBangla()>=50 && $this->getBangla()<60) {

            $a=3.00;//echo"Bangla&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getBangla() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B";
        }
        elseif($this->getBangla()>=40 && $this->getBangla()<50) {

            $a=2.5;//echo"Bangla&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getBangla() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C";
        }
        elseif($this->getBangla()>=33 && $this->getBangla()<40) {

            $a=2.00;//echo"Bangla&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getBangla() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D";
        }
        else{

            $a=0.00;//echo"Bangla&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getBangla() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;F";
        }
        }
        {
            if($this->getEnglish()>=80) {
                $b=5.00;//echo"English&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getEnglish() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A+";
            }
            elseif($this->getEnglish()>=70 && $this->getEnglish()<80) {

                $b=4.00;//echo"English&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getEnglish() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A";
            }
            elseif($this->getEnglish()>=60 && $this->getEnglish()<70) {

                $b=3.50;//echo"English&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getEnglish() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A-";
            }
            elseif($this->getEnglish()>=50 && $this->getEnglish()<60) {

                $b=3.00;//echo"English&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getEnglish() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B";
            }
            elseif($this->getEnglish()>=40 && $this->getEnglish()<50) {

                $b=2.50;//echo"English&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getEnglish() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C";
            }
            elseif($this->getEnglish()>=33 && $this->getEnglish()<40) {
                $b=2.00;//echo"English&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getEnglish() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D";
            }
            else{
                $b=0.00;//echo"English&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getEnglish() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;F";
            }
        }
        {
            if($this->getMath()>=80) {
                $c=5.00;//echo"Math&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getMath() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A+";
            }
            elseif($this->getMath()>=70 && $this->getMath()<80) {
                $c=4.00;//echo"Math&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getMath() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A";
            }
            elseif($this->getMath()>=60 && $this->getMath()<70) {
                $c=3.50;//echo"Math&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getMath() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A-";
            }
            elseif($this->getMath()>=50 && $this->getMath()<60) {
                $c=3.00;//echo"Math&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getMath() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B";
            }
            elseif($this->getMath()>=40 && $this->getMath()<50) {
                $c=2.50;//echo"Math&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getMath() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C";
            }
            elseif($this->getMath()>=33 && $this->getMath()<40) {
                $c=2.00;//echo"Math&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getMath() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D";
            }
            else{
                $c=0.00;//echo"Math&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getMath() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;F";
            }
        }
        {
            if($this->getIct()>=80) {
                $d=5.00;//echo"ICT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getIct() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A+";
            }
            elseif($this->getIct()>=70 && $this->getIct()<80) {
                $d=4.00;//echo"ICT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getIct() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A";
            }
            elseif($this->getIct()>=60 && $this->getIct()<70) {
                $d=3.50;//echo"ICT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getIct() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A-";
            }
            elseif($this->getIct()>=50 && $this->getIct()<60) {
                $d=3.00;//echo"ICT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getIct() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B";
            }
            elseif($this->getIct()>=40 && $this->getIct()<50) {
                $d=2.50;//echo"ICT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getIct() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C";
            }
            elseif($this->getIct()>=33 && $this->getIct()<40) {
                $d=2.00;//echo"ICT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getIct() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D";
            }
            else{
                $d=0.00;//echo"ICT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .$this->getIct() . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;F";
            }
        }
        if($a==0.00||$b==0.00||$c==0.00||$d==0.00){
            echo "<h3 style='color: white;'>SORRY!!! You are Failed. </h3>";
            echo "<h3 style='color: white;'>Your GPA is: </h3>";
            echo "<h3 style='color: white'> 0.00";
        }
        else{
            $x=($a+$b+$c+$d)/4;
            echo "<h3 style='color: white;'>Congratulations!!! You are Passed. </h3>";
            echo "<h3 style='color: white;'>Your GPA is: </h3>";
            echo "<h3 style='color: white'>".$x;
        }

    }
    public function personalinfo(){
        echo "<center>";
        echo "<p style='color: white'>Name:".$this->getName()."</p>";
        echo "<p style='color: white'>Student ID:".$this->getStudentID()."</p>";
        echo "<p style='color: white'>Date Of Birth:".$this->getDob()."</p>";
        echo "<p style='color: white'>Gender:".$this->getGender()."</p>";
        echo "</center>";
    }

    public function setData($postArray){
        if(array_key_exists("Name",$postArray)){
            $this->name=$postArray["Name"];
        }
        if(array_key_exists("StudentID",$postArray)){
            $this->studentID=$postArray["StudentID"];
        }
        if(array_key_exists("DOB",$postArray)){
            $this->dob=$postArray["DOB"];
        }
        if(array_key_exists("Gender",$postArray)){
            $this->gender=$postArray["Gender"];
        }
        if(array_key_exists("Bangla",$postArray)){
            $this->bangla=$postArray["Bangla"];
        }
        if(array_key_exists("English",$postArray)){
            $this->english=$postArray["English"];
        }
        if(array_key_exists("Math",$postArray)){
            $this->math=$postArray["Math"];
        }
        if(array_key_exists("Ict",$postArray)){
            $this->ict=$postArray["Ict"];
        }
    }
    public function getName()
    {
        return $this->name;
    }
    public function getStudentID()
    {
        return $this->studentID;
    }
    public function getDob()
    {
        return $this->dob;
    }
    public function getGender()
    {
        return $this->gender;
    }
    public function getBangla()
    {
        return $this->bangla;
    }
    public function getEnglish()
    {
        return $this->english;
    }
    public function getMath()
    {
        return $this->math;
    }
    public function getIct()
    {
        return $this->ict;
    }
}
?>
</div>
</body>
</html>
